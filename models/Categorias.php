<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property int $codigoEN
 * @property string $categoria
 *
 * @property Entrenadores $codigoEN0
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria'], 'required'],
            [['categoria'], 'string', 'max' => 40],
            [['codigoEN'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['codigoEN' => 'codigoEN']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoEN' => 'Codigo En',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[CodigoEN0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEN0()
    {
        return $this->hasOne(Entrenadores::className(), ['codigoEN' => 'codigoEN']);
    }
}
