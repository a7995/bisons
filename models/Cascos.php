<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cascos".
 *
 * @property int $codigoCA
 * @property string|null $marca
 * @property string|null $modelo
 * @property string|null $talla
 * @property float|null $precio
 * @property int|null $año_de_fabricación
 * @property int|null $codigoJU
 *
 * @property Jugadores $codigoJU0
 */
class Cascos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cascos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'number'],
            [['año_de_fabricación', 'codigoJU'], 'integer'],
            [['marca'], 'string', 'max' => 40],
            [['modelo'], 'string', 'max' => 80],
            [['talla'], 'string', 'max' => 4],
            [['codigoJU'], 'unique'],
            [['codigoJU'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigoJU' => 'codigoJU']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoCA' => 'Código de casco',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'talla' => 'Talla',
            'precio' => 'Precio',
            'año_de_fabricación' => 'Año De Fabricación',
            'codigoJU' => 'Codigo de jugador',
        ];
    }

    /**
     * Gets query for [[CodigoJU0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJU0()
    {
        return $this->hasOne(Jugadores::className(), ['codigoJU' => 'codigoJU']);
    }
}
