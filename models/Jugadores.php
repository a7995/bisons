<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $codigoJU
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $posicion
 * @property int|null $telefono
 *
 * @property Cascos $cascos
 * @property Corazas $corazas
 * @property Entrenan[] $entrenans
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono'], 'integer'],
            [['nombre'], 'string', 'max' => 40],
            [['apellidos'], 'string', 'max' => 80],
            [['posicion'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoJU' => 'Codigo Ju',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'posicion' => 'Posicion',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Cascos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCascos()
    {
        return $this->hasOne(Cascos::className(), ['codigoJU' => 'codigoJU']);
    }

    /**
     * Gets query for [[Corazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCorazas()
    {
        return $this->hasOne(Corazas::className(), ['codigoJU' => 'codigoJU']);
    }

    /**
     * Gets query for [[Entrenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenans()
    {
        return $this->hasMany(Entrenan::className(), ['codigoJU' => 'codigoJU']);
    }
}
