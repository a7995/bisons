<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materiales".
 *
 * @property int $codigoMA
 * @property string|null $marca
 * @property string|null $tipo
 * @property float|null $precio
 * @property string|null $estado
 *
 * @property Emplean[] $empleans
 */
class Materiales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materiales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'number'],
            [['marca', 'tipo'], 'string', 'max' => 40],
            [['estado'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoMA' => 'Código de material',
            'marca' => 'Marca',
            'tipo' => 'Tipo',
            'precio' => 'Precio',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Empleans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleans()
    {
        return $this->hasMany(Emplean::className(), ['codigoMA' => 'codigoMA']);
    }
}
