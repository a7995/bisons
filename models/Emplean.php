<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emplean".
 *
 * @property int $id
 * @property int $codigoEN
 * @property int $codigoMA
 *
 * @property Entrenadores $codigoEN0
 * @property Materiales $codigoMA0
 */
class Emplean extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emplean';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoEN', 'codigoMA'], 'required'],
            [['codigoEN', 'codigoMA'], 'integer'],
            [['codigoEN'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['codigoEN' => 'codigoEN']],
            [['codigoMA'], 'exist', 'skipOnError' => true, 'targetClass' => Materiales::className(), 'targetAttribute' => ['codigoMA' => 'codigoMA']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoEN' => 'Codigo En',
            'codigoMA' => 'Codigo Ma',
        ];
    }

    /**
     * Gets query for [[CodigoEN0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEN0()
    {
        return $this->hasOne(Entrenadores::className(), ['codigoEN' => 'codigoEN']);
    }

    /**
     * Gets query for [[CodigoMA0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMA0()
    {
        return $this->hasOne(Materiales::className(), ['codigoMA' => 'codigoMA']);
    }
}
