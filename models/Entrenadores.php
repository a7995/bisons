<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $codigoEN
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $antiguedad
 * @property int|null $telefono
 * @property int|null $titulado
 *
 * @property Categorias[] $categorias
 * @property Emplean[] $empleans
 * @property Entrenan[] $entrenans
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['antiguedad', 'telefono', 'titulado'], 'integer'],
            [['nombre'], 'string', 'max' => 40],
            [['apellidos'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoEN' => 'Codigo En',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'antiguedad' => 'Antiguedad',
            'telefono' => 'Telefono',
            'titulado' => 'Titulado',
        ];
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(Categorias::className(), ['codigoEN' => 'codigoEN']);
    }

    /**
     * Gets query for [[Empleans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleans()
    {
        return $this->hasMany(Emplean::className(), ['codigoEN' => 'codigoEN']);
    }

    /**
     * Gets query for [[Entrenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenans()
    {
        return $this->hasMany(Entrenan::className(), ['codigoEN' => 'codigoEN']);
    }
}
