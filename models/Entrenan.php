<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenan".
 *
 * @property int $id
 * @property int $codigoEN
 * @property int $codigoJU
 *
 * @property Entrenadores $codigoEN0
 * @property Jugadores $codigoJU0
 */
class Entrenan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoEN', 'codigoJU'], 'required'],
            [['codigoEN', 'codigoJU'], 'integer'],
            [['codigoEN'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['codigoEN' => 'codigoEN']],
            [['codigoJU'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigoJU' => 'codigoJU']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoEN' => 'Codigo En',
            'codigoJU' => 'Codigo Ju',
        ];
    }

    /**
     * Gets query for [[CodigoEN0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEN0()
    {
        return $this->hasOne(Entrenadores::className(), ['codigoEN' => 'codigoEN']);
    }

    /**
     * Gets query for [[CodigoJU0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJU0()
    {
        return $this->hasOne(Jugadores::className(), ['codigoJU' => 'codigoJU']);
    }
}
