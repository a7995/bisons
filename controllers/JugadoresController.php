<?php

namespace app\controllers;

use app\models\Jugadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JugadoresController implements the CRUD actions for Jugadores model.
 */
class JugadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Jugadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoJU' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jugadores model.
     * @param int $codigoJU Codigo Ju
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoJU)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoJU),
        ]);
    }

    /**
     * Creates a new Jugadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Jugadores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoJU' => $model->codigoJU]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Jugadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoJU Codigo Ju
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoJU)
    {
        $model = $this->findModel($codigoJU);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoJU' => $model->codigoJU]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Jugadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoJU Codigo Ju
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoJU)
    {
        $this->findModel($codigoJU)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jugadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoJU Codigo Ju
     * @return Jugadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoJU)
    {
        if (($model = Jugadores::findOne(['codigoJU' => $codigoJU])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
