<?php

namespace app\controllers;

use app\models\Cascos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CascosController implements the CRUD actions for Cascos model.
 */
class CascosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Cascos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Cascos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoCA' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cascos model.
     * @param int $codigoCA Codigo Ca
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoCA)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoCA),
        ]);
    }

    /**
     * Creates a new Cascos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Cascos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoCA' => $model->codigoCA]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cascos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoCA Codigo Ca
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoCA)
    {
        $model = $this->findModel($codigoCA);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoCA' => $model->codigoCA]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cascos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoCA Codigo Ca
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoCA)
    {
        $this->findModel($codigoCA)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cascos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoCA Codigo Ca
     * @return Cascos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoCA)
    {
        if (($model = Cascos::findOne(['codigoCA' => $codigoCA])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
