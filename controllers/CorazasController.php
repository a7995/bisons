<?php

namespace app\controllers;

use app\models\Corazas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CorazasController implements the CRUD actions for Corazas model.
 */
class CorazasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Corazas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Corazas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoCO' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Corazas model.
     * @param int $codigoCO Codigo Co
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoCO)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoCO),
        ]);
    }

    /**
     * Creates a new Corazas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Corazas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoCO' => $model->codigoCO]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Corazas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoCO Codigo Co
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoCO)
    {
        $model = $this->findModel($codigoCO);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoCO' => $model->codigoCO]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Corazas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoCO Codigo Co
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoCO)
    {
        $this->findModel($codigoCO)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Corazas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoCO Codigo Co
     * @return Corazas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoCO)
    {
        if (($model = Corazas::findOne(['codigoCO' => $codigoCO])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
