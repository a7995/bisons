<?php

namespace app\controllers;

use app\models\Entrenadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EntrenadoresController implements the CRUD actions for Entrenadores model.
 */
class EntrenadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Entrenadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Entrenadores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoEN' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entrenadores model.
     * @param int $codigoEN Codigo En
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoEN)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoEN),
        ]);
    }

    /**
     * Creates a new Entrenadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Entrenadores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoEN' => $model->codigoEN]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entrenadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoEN Codigo En
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoEN)
    {
        $model = $this->findModel($codigoEN);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoEN' => $model->codigoEN]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entrenadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoEN Codigo En
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoEN)
    {
        $this->findModel($codigoEN)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entrenadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoEN Codigo En
     * @return Entrenadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoEN)
    {
        if (($model = Entrenadores::findOne(['codigoEN' => $codigoEN])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
