<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
//use app\models\Cascos;
//use app\models\Corazas;
//use app\models\Jugadores;
//use app\models\Materiales;

class SiteController extends Controller
{
     public function actionConsultamaterial(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT codigoCA AS CodigoCasco, talla FROM cascos 
                                                        WHERE codigoJU IS NULL
                                                        UNION 
                                                        SELECT "CodigoCoraza", "Talla"
                                                        UNION
                                                        SELECT codigoCO AS CodigoCoraza, talla FROM corazas
                                                        WHERE codigoJU IS NULL'
            
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['CodigoCasco', 'talla'],
            "titulo"=>"Material disponible",
            "enunciado"=>"Cascos y corazas en desuso",
        ]);
     }
     
     public function actionConsultamaterialcampo(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT codigoMA, tipo FROM materiales WHERE estado = "Bueno"'
            
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['codigoMA', 'tipo'],
            "titulo"=>"Material de campo disponible",
            "enunciado"=>"Materiales de campo en buen estado",
        ]);
     }
     
     public function actionConsultaasociar(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT jugadores.codigoJU, nombre, apellidos, telefono, codigoCA, codigoCO FROM jugadores
                                                            INNER JOIN cascos ON cascos.codigoJU=jugadores.codigoJU
                                                            INNER JOIN corazas ON corazas.codigoJU=jugadores.codigoJU'
             
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['codigoJU', 'nombre', 'apellidos', 'telefono', 'codigoCA', 'codigoCO'],
            "titulo"=>"Cascos y corazas",
            "enunciado"=>"Jugadores con sus cascos y corazas",
        ]);
     }
     
     public function actionConsultaestadoma(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT codigoMA, tipo, estado FROM materiales 
                                                     WHERE estado="roto"'
            
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['codigoMA', 'tipo', 'estado'],
            "titulo"=>"Material de campo",
            "enunciado"=>"Material en mal estado",
        ]);
     }
     
     public function actionConsultaprecio(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT (SELECT DISTINCT SUM(precio) AS TotalCascos FROM cascos) 
             + (SELECT DISTINCT SUM(precio) AS TotalCorazas FROM corazas) 
             + (SELECT DISTINCT SUM(precio) AS TotalMaterial FROM materiales) AS TotalInvertido'
            
        ]);
        $dataProvider2 = new SqlDataProvider(['sql'=>'SELECT DISTINCT SUM(precio) AS TotalCascos FROM cascos'
            
        ]);
        
        $dataProvider3 = new SqlDataProvider(['sql'=>'SELECT DISTINCT SUM(precio) AS TotalCorazas FROM corazas'
            
        ]);
        
        $dataProvider4 = new SqlDataProvider(['sql'=>'SELECT DISTINCT SUM(precio) AS TotalMaterial FROM materiales'
            
        ]);
        
        return $this->render("inversiones",[
            "resultados"=>$dataProvider,
            "campos"=>['TotalInvertido'],
            "titulo"=>"Dinero Invertido",
            "enunciado"=>"Cantidad total invertida",
            "resultados2"=>$dataProvider2,
            "resultados3"=>$dataProvider3,
            "resultados4"=>$dataProvider4
        ]);
     }
     
     public function actionConsultaprecio2(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT SUM(precio) AS TotalCascos FROM cascos'
            
        ]);
        return $this->render("inversiones",[
            "resultados2"=>$dataProvider,
            "campos"=>['TotalCascos'],
            "titulo"=>"Dinero Invertido",
            "enunciado"=>"Cantidad total invertida",
        ]);
     }
     
     public function actionPosicion1(){
         $dataProvider = new SqlDataProvider(['sql'=>'SELECT COUNT(posicion) cantidad, posicion FROM jugadores GROUP BY posicion']);
       
         
         return $this->render("posiciones",[
             "dataProvider"=>$dataProvider,
         ]);
     }
     
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    
}
