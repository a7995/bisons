<?php

use yii\widgets\ListView;
use yii\helpers\Html;
?>
<div class="site-index ">
    
    <div class="body-content ">
       
        <div class="pt-5"> </div>
         <div ><h3 class="titulos">INVERSIÓN DEL CLUB</h3></div>
        <div class="row margenizquierda">
            <br>
            <?=
        ListView::widget([
            'dataProvider' => $resultados4,
            'layout' => "{items}",
            'itemView' => function($model) {
                ?>
                <div class="col-sm-6">
                <div class="cardpos"> 
                    <h3>Total en Material</h3>
                    
                    <?=
                    $model['TotalMaterial'];
                    ?>
                </div>
                </div>
            <?php }]); ?>
          <?=
        ListView::widget([
            'dataProvider' => $resultados3,
            'layout' => "{items}",
            'itemView' => function($model) {
                ?>
                <div class="col-sm-6">  
                <div class="cardpos"> 
                    <h3>Total en Corazas</h3>
                    <?=
                    $model['TotalCorazas'];
                    ?>
                </div>
                </div>
            <?php }]); ?>
            
            <?=
        ListView::widget([
            'dataProvider' => $resultados2,
            'layout' => "{items}",
            'itemView' => function($model) {
                ?>
                <div class="col-sm-6">
                <div class="cardpos"> 
                    <h3>Total en Cascos</h3>
                    <?=
                    $model['TotalCascos'];
                    ?>
                </div>
                </div>
            <?php }]); ?>
             <?=
        ListView::widget([
            'dataProvider' => $resultados,
            'layout' => "{items}",
            'itemView' => function($model) {
                ?>
                <div class="col-sm-6">  
                <div class="cardpos"> 
                    <h3>Total Invertido</h3>
                    <?=
                    $model['TotalInvertido'];
                    ?>
                </div>
                </div>
            <?php }]); ?>
    </div>
</div>
</div>
