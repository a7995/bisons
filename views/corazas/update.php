<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Corazas */

$this->title = 'Update Corazas: ' . $model->codigoCO;
$this->params['breadcrumbs'][] = ['label' => 'Corazas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoCO, 'url' => ['view', 'codigoCO' => $model->codigoCO]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="corazas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
