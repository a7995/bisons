<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Corazas */

$this->title = 'Create Corazas';
$this->params['breadcrumbs'][] = ['label' => 'Corazas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="corazas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
