<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */

$this->title = 'Update Entrenadores: ' . $model->codigoEN;
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoEN, 'url' => ['view', 'codigoEN' => $model->codigoEN]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entrenadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
