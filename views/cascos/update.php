<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cascos */

$this->title = 'Update Cascos: ' . $model->codigoCA;
$this->params['breadcrumbs'][] = ['label' => 'Cascos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoCA, 'url' => ['view', 'codigoCA' => $model->codigoCA]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cascos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
