<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */

$this->title = 'Update Jugadores: ' . $model->codigoJU;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoJU, 'url' => ['view', 'codigoJU' => $model->codigoJU]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jugadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
