<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Emplean */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emplean-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoEN')->textInput() ?>

    <?= $form->field($model, 'codigoMA')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
