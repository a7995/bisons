<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emplean */

$this->title = 'Create Emplean';
$this->params['breadcrumbs'][] = ['label' => 'Empleans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emplean-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
